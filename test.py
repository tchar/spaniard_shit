#!/usr/bin/python

import hmac
import hashlib
from base64 import b64encode as b64enc, b64decode as b64dec
from pyDes import triple_des, CBC, PAD_PKCS5


def step1(key):
	return b64dec(key)

def step2(data):
	return b64dec(data)

def step3(key, m_order):
	c = triple_des(key, CBC, "\0\0\0\0\0\0\0\0", pad="\0")
	return c.encrypt(m_order)

def step4(data, key):
	dig = hmac.new(key, msg=data, digestmod=hashlib.sha256).digest()
	return b64enc(dig)

def encrypt(msg):
	key = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'
	dig = hmac.new('sq7HjrUOBfKmC576ILgskD5srU870gJ7', msg=msg, digestmod=hashlib.sha256).digest()
	return b64enc(dig)

key = "sq7HjrUOBfKmC576ILgskD5srU870gJ7"
data = '{"DS_MERCHANT_AMOUNT":"145","DS_MERCHANT_ORDER":"1442772646","DS_MERCHANT_MERCHANTCODE":"343801064","DS_MERCHANT_CURRENCY":"978","DS_MERCHANT_TRANSACTIONTYPE":"0","DS_MERCHANT_TERMINAL":"1","DS_MERCHANT_MERCHANTURL":"b2b.onetourismo.com","DS_MERCHANT_URLOK":"https://google.com","DS_MERCHANT_URLKO":"https://facebook.com"}'
data_enc = b64enc(data)
order = "1442772646"

key_dec = step1(key)
data_dec = step2(data_enc)
s3 = step3(key_dec, order)
s4 = step4(data_enc, s3)
print(s4)
print(data_enc)

